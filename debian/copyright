Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/ROCm/MIOpen
Upstream-Name: MIOpen
Files-Excluded: src/kernels/*.kdb.bz2
                src/kernels/*.db.bz2
                src/kernels/*.fdb.txt
                src/kernels/*.inc
                src/kernels/*.s
                .gitattributes
Comment: These compressed files contain compiled kernel codes which will be queried
 when users invoke a kernel for the first time. Currently we don't know how these
 files are generated and maintained, so exclude them out of the source package.
 .
 The .gitattributes is used for Git LFS recording, here we exclude it as well to
 disable track these LFS files.
 .
 The fdb.txt files are Find database for MIOpen immediate mode, which are generated
 by a list of popular kernels on machines with AMD GPUs. We cannot form or reproduce
 them when buidling this package, thus exclude.
 .
 There exist some assembly codes (*.s, *.inc) used as MIOpen kernels, but they are
 disassembled from AMD proprietory internal sources, so we also exclude them.

Files: *
Copyright: 2017-2022, Advanced Micro Devices, Inc.
License: Expat

Files: debian/*
Copyright: 2023, Cordell Bloor <cgmb@slerp.xyz>
           2024, Xuanteng Huang <xuanteng.huang@outlook.com>
License: Expat

Files: driver/mloSoftmaxHost.hpp
Copyright: 2017, Advanced Micro Devices, Inc.
License: BSD-2-clause

Files: fin/src/base64.cpp
Copyright: 2004-2017, 2020, René Nyffenegger
License: Zlib

Files: src/include/miopen/kernel_cache.hpp
Copyright: 2017, Advanced Micro Devices, Inc.
License: Apache-2.0 and Expat

Files: src/include/miopen/mlo_internal.hpp
Copyright: 2017, Advanced Micro Devices, Inc.
License: BSD-2-clause and Expat

Files: src/kernel_cache.cpp
Copyright: 2017, Advanced Micro Devices, Inc.
License: Apache-2.0 and Expat

Files: src/md5.cpp
Copyright: 2001, Alexander Peslyak
License: public-domain
 No copyright is claimed, and the software is hereby placed in the public
 domain. In case this attempt to disclaim copyright and place the software in
 the public domain is deemed null and void, then the software is Copyright (c)
 2001 Alexander Peslyak and it is hereby released to the general public under
 the following terms:
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted.
 .
 There's ABSOLUTELY NO WARRANTY, express or implied.
 .
 (This is a heavily cut-down "BSD license".)

License: Expat
 MIT License
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Zlib
 This source code is provided 'as-is', without any express or implied
 warranty. In no event will the author be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this source code must not be misrepresented; you must not
    claim that you wrote the original source code. If you use this source code
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original source code.
 .
 3. This notice may not be removed or altered from any source distribution.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without modification, are permitted
 provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this list of conditions and
 the following disclaimer.
 Redistributions in binary form must reproduce the above copyright notice, this list of conditions
 and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
  OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, a copy of the above license can be found at
 .
     /usr/share/common-licenses/Apache-2.0
