#!/bin/bash
# autopkgtest driver for ROCm upstream binaries
#
# Expects argument "$1" to be the name of a sub-directory, such that the
# following following directory exists:
#
#   /usr/libexec/rocm/$1
#
# Will run all executables in that directory, and exit with status=1 if
# any failure occurred, otherwise with status=0. A failure is defined as an
# executable exiting with a status != 0.


TESTSDIR="/usr/libexec/rocm/$1"

if [ ! -e /dev/kfd ]
then
	echo "/dev/kfd not present, system either lacks AMD GPU or AMDGPU driver is not loaded."
	echo "Skipping tests."
	# Magic number to signal 'skipped'
	exit 77
elif [ "$(id -u)" != "0" ] && [ ! -r /dev/kfd ]
then
	echo "/dev/kfd present but no read permission."
	echo "Skipping tests."
	exit 77
elif [ -z "$1" ]
then
	echo "This script needs to be called with a directory name as an argument." >&2
	echo "Aborting." >&2
	exit 1
elif [ ! -d "$TESTSDIR" ]
then
	echo "The directory $TESTSDIR does not exist." >&2
	echo "Aborting." >&2
	exit 1
fi

# 16 = testbed failure
cd "$AUTOPKGTEST_TMP" || exit 16

# First, gather system info
mount -t debugfs none /sys/kernel/debug || true
if [ -d /sys/kernel/debug/dri ]
then
	for index in $(ls /sys/kernel/debug/dri)
	do
		info="/sys/kernel/debug/dri/$index/amdgpu_firmware_info"
		if [ -f "$info" ]
		then
			# shellcheck disable=SC2024   # we don't need privileged write
			cat "$info" > "$AUTOPKGTEST_ARTIFACTS/amdgpu_firmware_info.$index"
		fi
	done
else
	echo "Could not read /sys/kernel/debug/dri" >> "$AUTOPKGTEST_ARTIFACTS/firmware.err"
fi
# shellcheck disable=SC2024   # we don't need privileged write
dmesg > "$AUTOPKGTEST_ARTIFACTS/dmesg.before" || true

# Any individual failure is overall failure
EXITCODE=0
for TESTNAME in $TESTSDIR/test_*; do
	$TESTNAME --all --verbose --time || EXITCODE=1
done

# Test with MIOpen driver
TEST_SUITES="conv pool lrn activ softmax bnorm rnn tensorop reduce"
for SUITE in ${TEST_SUITES}; do
	${TESTSDIR}/MIOpenDriver ${SUITE} -V 1 -t 1 || EXITCODE=1
	${TESTSDIR}/MIOpenDriver "${SUITE}fp16" -V 1 -t 1 || EXITCODE=1
done
${TESTSDIR}/MIOpenDriver ctc -V 1 -t 1 || EXITCODE=1

# Tests might have generated new messages
# shellcheck disable=SC2024   # we don't need privileged write
dmesg > "$AUTOPKGTEST_ARTIFACTS/dmesg.after" || true

exit $EXITCODE
