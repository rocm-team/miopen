#!/usr/bin/make -f

export CXX=clang++-17
export DEB_BUILD_MAINT_OPTIONS = hardening=+all optimize=-lto
export DEB_CXXFLAGS_MAINT_PREPEND = -gz
export VERBOSE=1

# filter incompatible options from affecting device code
CXXFLAGS := $(subst -fstack-protector-strong,-Xarch_host -fstack-protector-strong,$(CXXFLAGS))
CXXFLAGS := $(subst -fcf-protection,-Xarch_host -fcf-protection,$(CXXFLAGS))

NUM_THREADS = 1
ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
	NUM_THREADS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
endif

CMAKE_FLAGS = \
	-DCMAKE_BUILD_TYPE=Release \
	-DROCM_SYMLINK_LIBS=OFF \
	-DBUILD_FILE_REORG_BACKWARD_COMPATIBILITY=OFF \
	-DMIOPEN_USE_COMPOSABLEKERNEL=OFF \
	-DMIOPEN_USE_MLIR=OFF \
	-DMIOPEN_USE_HIPRTC=ON \
	-DMIOPEN_BUILD_DRIVER=ON \
	-DMIOPEN_ENABLE_FIN=OFF \
	-DMIOPEN_STRIP_SYMBOLS=OFF \
	-DMIOPEN_DISABLE_SYSDB=ON \
	-DBoost_USE_STATIC_LIBS=OFF

%:
	dh $@ -Scmake --builddirectory=build

override_dh_auto_configure:
	dh_auto_configure -- $(CMAKE_FLAGS)

override_dh_install:
	mkdir -p $(CURDIR)/debian/tmp/usr/bin && install $(CURDIR)/build/bin/test_* $(CURDIR)/debian/tmp/usr/bin
	install $(CURDIR)/build/bin/addkernels $(CURDIR)/debian/tmp/usr/bin
	@for exe in `ls -l $(CURDIR)/debian/tmp/usr/bin/test_*`; do \
		chrpath -d $$exe; \
	done
	dh_install

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	set -e \
	; if [ -r /dev/kfd ] \
	; then echo "W: No tests." \
	; else echo "W: /dev/kfd unreadable: no available AMD GPU." \
	;      echo "W: tests skipped." \
	; fi
endif

# dwz doesn't fully support DWARF-5 yet, see #1016936
override_dh_dwz:
	:
